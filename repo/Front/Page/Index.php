<?php //-->
/*
 * This file is part of the Openovate Labs Inc. framework library
 * (c) 2013-2014 Openovate Labs
 *
 * Copyright and license information can be found at LICENSE
 * distributed with this package.
 */

namespace Front\Page;

use Model\User as User;
use Dao\UserDao as UserDao;

/**
 * The base class for any class that defines a view.
 * A view controls how templates are loaded as well as
 * being the final point where data manipulation can occur.
 *
 * @vendor Openovate
 * @package Framework
 */
class Index extends \Page
{
	protected $title = 'Index';
	protected $body;
	protected $userDao;

	public function getVariables()
	{
    $userDao = new UserDao();

		$users = $userDao -> getAllUsers();

		$postVars = control()->registry()->get(false, 'post');

		$passwordChanged = null;
		if (isset($postVars['username']) && isset($postVars['oldPW'])
			&& isset($postVars['newPW'])){
			$changedPassword = $this->changePassword($users, $postVars['username'],
        $postVars['oldPW'], $postVars['newPW']);

			$passwordChanged = $changedPassword['passwordChanged'];
			if($passwordChanged) {
        $userDao->changePassword($postVars['username'], $postVars['newPW']);
			}
		}

		$body = array(
			'users' 		      => $users,
			'passwordChanged' => $passwordChanged
		);

		return $body;
	}

	public function changePassword($users, $username, $oldPW, $newPW)
	{
		foreach($users as $key => $value) {
			if($value->getUsername() == $username) {
				$passwordChanged = $users[$key]->changePassword($oldPW, $newPW);
			}
		}

		return array(
			'passwordChanged' => $passwordChanged,
			'users'			  => $users
		);
	}

}
