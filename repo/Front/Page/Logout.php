<?php //-->

namespace Front\Page;

class Logout extends \Page
{
  protected $title = 'Logout';

  public function getVariables()
  {
    unset($_SESSION['loggedInUser']);

    return array(
    	'successful' => true
    );
  }

}
