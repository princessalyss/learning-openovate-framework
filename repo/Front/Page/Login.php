<?php //-->

namespace Front\Page;

use Dao\UserDao as UserDao;

class Login extends \Page
{
  protected $title = 'Login';
  protected $body;
  protected $loggedIn;

  protected $userDao;

  public function getVariables()
  {
    if(isset($_SESSION['loggedInUser'])) {
      return array(
      	'alreadyLoggedIn' => true,
      	'successful'   => false
      );
    }

    if(isset($_POST['username']) && isset($_POST['password'])) {
      $username = $_POST['username'];
      $password = $_POST['password'];

      $processLogin = $this->loginUser($username, $password);
      if($processLogin == true) {
        return array(
          'alreadyLoggedIn' => false,
          'successful'   => true
        );
      } else {
        return array(
        	'alreadyLoggedIn' => false,
          'successful'   => false
        );
      }
    } else {
      return array(
        'alreadyLoggedIn' => false,
        'successful'   => false
      );
    }
  }

  private function loginUser($username, $password)
  {
    $userDao = new UserDao();
    $user = $userDao->getUser($username, $password);

    if($user != null && $user->isPasswordValid($password)) {
      $loggedIn = true;
      $_SESSION['loggedInUser'] = serialize($user);
      return true;
    }

    return false;
  }

	public function render()
	{
    $messages = array();
    if(isset($_SESSION['messages'])) {
      $messages = $_SESSION['messages'];
      $_SESSION['messages'] = array();
    }

		$path = control()->path('template');
		$template = '/login.php';

		$helpers = $this->getHelpers();

		$body = array_merge($helpers, $this->getVariables());

		$file = $path.$template;

		return control()->trigger('body')->template($file, $body);
	}

}
