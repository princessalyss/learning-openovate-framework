<?php if(isset($_GET['login']) && $_GET['login'] == 'false'): ?>Wrong Login<hr /><?php endif; ?>
<?php if(isset($_GET['login']) && $_GET['login'] == 'true'): ?>Already Logged In<hr /><?php endif; ?>

<?php if(isset($_SESSION['loggedInUser'])): ?>

<h1>Logged in as <?php echo unserialize($_SESSION['loggedInUser'])->getUsername(); ?></h1>
<a href="/logout">Logout</a>
<?php else: ?>
<form action="/login" method="post">
  <label for="username">Username</label><br />
  <input type="text" name="username" />
  <br />
  <label for="password">Password</label><br />
  <input type="password" name="password" />
  <br />
  <input type="submit" name="Login" value="Login" />
</form>
<?php endif; ?>

<hr />
<table>
  <tr>
    <th>Username</th>
    <th>Password</th>
<?php foreach($users as $key => $user): ?>
	<tr>
		<td><?php echo $user->getUsername(); ?></td>
		<td><?php echo $user->getPassword(); ?></td>
	</tr>
<?php endforeach; ?>
</table>
<br />
<a href="http://admin.user-eden/">Admin Site</a>
