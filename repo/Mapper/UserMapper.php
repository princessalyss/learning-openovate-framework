<?php //-->
	namespace Mapper;

	use Model\User as User;

	class UserMapper
	{
		public static function map($row)
		{
      $user = null;
			if(isset($row['username']) && isset($row['password'])) {
        $user = new User($row['username'], $row['password']);

        if(isset($row['firstname'])) {
          $user -> setFirstName($row['firstname']);
        }
        if(isset($row['lastname'])) {
          $user -> setLastName($row['lastname']);
        }
			}

			return $user;
		}
	}
