<?php //-->
  namespace Dao;

  use Model\User as User;
  use Mapper\UserMapper as UserMapper;

  class UserDao extends \Control
  {
    protected $database = null;

    public function getAllUsers()
    {
      $database = control()->database('mysql');

      $results = $database->search('users')->getRows();

      $users = array();

      foreach($results as $key => $row) {
        $user = UserMapper::map($row);

        if($user != null) {
          array_push($users, $user);
        }
      }

      return $users;
    }

    public function changePassword($username, $password)
    {
      $database = control()->database('mysql');

      $collection = $database->search('users')
        ->filterByUsername($username)
        ->getCollection()
        ->setPassword($password)
        ->save();
    }

    public function getUser($username)
    {
      $database = control()->database('mysql');

      $result = $database->getRow('users', 'username', $username);

      $user = UserMapper::map($result);

      return $user;
    }
  }
