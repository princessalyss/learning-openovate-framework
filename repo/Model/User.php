<?php //-->
namespace Model;

class User
{
	protected $username = null;
	protected $password = null;
	protected $firstName = null;
	protected $lastName = null;

	public function __construct($username, $password)
	{
		$this->username = $username;
		$this->password = $password;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function getFirstName()
	{
    return $this->firstName;
	}

	public function getLastName()
	{
    return $this->lastName;
	}

	public function setFirstName($firstName)
	{
    $this->firstName = $firstName;
    return $this;
	}

	public function setLastName($lastName)
	{
    $this->lastName = $lastName;
    return $this;
	}

	public function changePassword($oldPassword, $newPassword)
	{
		if($oldPassword == $this->password) {
			$this->password = $newPassword;
			return true;
		}
		return false;
	}

	public function isPasswordValid($password)
	{
		return $this->password == $password;
	}

	public function __toString()
	{
		return $this-> username.' | '.$this-> password;
	}

}
